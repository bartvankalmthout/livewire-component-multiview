<?php

namespace Database\Seeders;

use App\Models\Invoice;
use Illuminate\Database\Seeder;

class InvoiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 15000; $i++) {
            Invoice::create([
                'name' => 'Invoice ' . $i,
                'amount' => 100 + $i,
                'due_date' => now(),
            ]);
        }
    }
}
