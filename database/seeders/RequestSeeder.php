<?php

namespace Database\Seeders;

use App\Models\Request;
use Illuminate\Database\Seeder;

class RequestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 15000; $i++) {
            Request::create([
                'domain' => 'fakecompany' . $i . '.nl',
                'brand' => 'SuperCoolBrand',
            ]);
        }
    }
}
