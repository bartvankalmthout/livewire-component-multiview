<?php

namespace Database\Seeders;

use App\Models\Certificate;
use Illuminate\Database\Seeder;

class CertificateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 15000; $i++) {
            Certificate::create([
                'common_name' => 'supercooldomain' . $i . '.nl'
            ]);
        }
    }
}
