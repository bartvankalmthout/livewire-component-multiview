<?php

namespace App\Http\Controllers;

use App\Models\Certificate;
use App\Models\Invoice;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

class ResourceController extends Controller
{
    public function __invoke(Request $request, string $resource)
    {
        $limit = $request->get('limit', 24);
        $offset = $request->get('offset', 0);

        /** @var Model $resourceClass */
        $resourceClass = match ($resource) {
             'certificates' => Certificate::class,
             'invoices' => Invoice::class,
             'requests' => \App\Models\Request::class,
            default => null,
        };

        if($resourceClass == null) {
            throw new ResourceNotFoundException('The requested resource doesn\'t exist.');
        }

        $results = $resourceClass::Query()
            ->limit($limit)
            ->offset($offset)
            ->get();

        return $results;
    }
}
