<?php

namespace App\Http\Livewire;

use App\Models\Certificate;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\Facades\DB;

class CertificatesTable extends Table
{
    function getNewPageFromDataProvider()
    {
        $client = new Client(['base_uri' => 'http://host.docker.internal:8017/api/']);
        $json = $client->get('certificates', [
           RequestOptions::QUERY => [
               'offset' => $this->offset,
               'limit' => $this->limit,
           ],
            RequestOptions::TIMEOUT => 10,
        ]);

        $items = json_decode($json->getBody()->getContents(), true);
        $collection = collect([]);

        foreach($items as $item) {
            $collection[] = new Certificate($item);
        }

        $this->results = $collection;
        $this->numberOfResults = 15000;
    }

    function getHeaderRowViewProperty()
    {
        return 'tables.certificates.header-row';
    }

    function getBodyRowViewProperty()
    {
        return 'tables.certificates.body-row';
    }
}
