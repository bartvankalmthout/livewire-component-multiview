<?php

namespace App\Http\Livewire;

use Livewire\Component;

abstract class Table extends Component
{
    public $limit = 24;
    public $offset = 0;
    public $numberOfResults;
    public $results;

    const PAGE_SIZE = 24;

    abstract function getHeaderRowViewProperty();
    abstract function getBodyRowViewProperty();

    abstract function getNewPageFromDataProvider();

    public function mount() {
        $this->getNewPageFromDataProvider();
    }

    public function goToPreviousPage() {
        $this->offset = max($this->offset - self::PAGE_SIZE, 0);
        $this->refeshResults();
    }

    public function goToNextPage() {
        $this->offset = min($this->offset + self::PAGE_SIZE, ($this->numberOfResults - self::PAGE_SIZE));
        $this->refeshResults();
    }

    public function refeshResults() {
        $this->getNewPageFromDataProvider();
    }

    public function getCurrentPageProperty() {
        return max(ceil($this->offset / self::PAGE_SIZE), 1);
    }

    public function getTotalPagesProperty() {
        return ceil(($this->numberOfResults / self::PAGE_SIZE)) - 1;
    }

    public function render()
    {
        return view('livewire.table');
    }
}
