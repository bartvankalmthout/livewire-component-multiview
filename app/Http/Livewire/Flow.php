<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Flow extends Component
{
    public $fields;
    public $view;

    public function mount() {
        $this->view = 'livewire.flow-option1';
    }

    public function toggleViews() {
        if($this->view == 'livewire.flow-option1') {
            $this->view = 'livewire.flow-option2';
        } else {
            $this->view = 'livewire.flow-option1';
        }
    }

    public function render()
    {
        return view($this->view);
    }
}
