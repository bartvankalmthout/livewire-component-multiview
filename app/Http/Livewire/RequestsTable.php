<?php

namespace App\Http\Livewire;

use App\Models\Request;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Database\Eloquent\Model;

class RequestsTable extends Table
{
    function getNewPageFromDataProvider()
    {
        $client = new Client(['base_uri' => 'http://host.docker.internal:8017/api/']);
        $json = $client->get('requests', [
            RequestOptions::QUERY => [
                'offset' => $this->offset,
                'limit' => $this->limit,
            ],
            RequestOptions::TIMEOUT => 10,
        ]);

        $items = json_decode($json->getBody()->getContents(), true);
        $collection = collect([]);

        foreach($items as $item) {
            $collection[] = new Request($item);
        }

        $this->results = $collection;
        $this->numberOfResults = 15000;
    }

    function getHeaderRowViewProperty()
    {
        return 'tables.requests.header-row';
    }

    function getBodyRowViewProperty()
    {
        return 'tables.requests.body-row';
    }
}
