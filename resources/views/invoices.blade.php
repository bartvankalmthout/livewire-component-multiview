<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">



        @livewireStyles
    </head>
    <body class="antialiased">
        <div class="">
            @include('menu')
            <div class="mx-auto sm:px-6 lg:px-8">
                    <livewire:invoices-table />
            </div>
        </div>
        @livewireScripts
    </body>
</html>
