<div class="p-2 center ml-0 absolute w-1/4">
    <a class="text-indigo-400 hover:text-blue-800 hover:underline px-1" href="{{ url('/certificates') }}">Certificates</a>
    <a class="text-indigo-400 hover:text-blue-800 hover:underline px-1" href="{{ url('/invoices') }}">Invoices</a>
    <a class="text-indigo-400 hover:text-blue-800 hover:underline px-1" href="{{ url('/requests') }}">Requests</a>
</div>
