<div>
    <div class="border-gray-700 border-2 rounded-sm p-10">
        <input wire:model="fields.text" type="text"
               class="center p-2 mb-3 border-gray-900 border-2 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md">
        <div class="border-1 m-1 center border-dotted border-gray-50 text-center">
            {{ $fields['text']  ?? '' }}
        </div>

        @include('flow-footer')
    </div>
</div>
