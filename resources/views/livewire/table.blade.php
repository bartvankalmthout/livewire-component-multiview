<div class="center mt-2">
    <table class="table-auto border-1 border-gray-200 mx-auto">
        <thead>
            @include($this->headerRowView)
        </thead>
        <tbody>
            @foreach($this->results as $item)
                @include($this->bodyRowView, ['item' => $item])
            @endforeach
        </tbody>
    </table>

    <div class="text-center center pb-2 pt-1">
        <span>Showing 24 of {{ $this->numberOfResults }}</span> <br>
        <button wire:click="goToPreviousPage" class="mt-5 inline-flex items-center px-4 py-2 bg-indigo-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-indigo-700 active:bg-indigo-900 focus:outline-none focus:border-indigo-900 focus:ring focus:ring-indigo-300 disabled:opacity-25 transition">
            Previous
        </button>
        <span class="px-3">{{ $this->currentPage }} / {{ $this->totalPages }}</span>
        <button wire:click="goToNextPage" class="mt-5 inline-flex items-center px-4 py-2 bg-indigo-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-indigo-700 active:bg-indigo-900 focus:outline-none focus:border-indigo-900 focus:ring focus:ring-indigo-300 disabled:opacity-25 transition">
            Next
        </button>
    </div>
</div>
