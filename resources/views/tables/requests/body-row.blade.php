<tr class="odd:bg-indigo-200 border border-indigo-500 last:border-b-0 p-2">
    <td class="p-1 pl-2.5">{{ $item->id }}</td>
    <td class="p-1">{{ $item->domain }}</td>
    <td class="p-1">{{ $item->brand }}</td>
    <td class="p-1">{{ $item->created_at }}</td>
    <td class="p-1 pr-2.5">{{ $item->updated_at }}</td>
</tr>
